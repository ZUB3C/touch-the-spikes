import pygame
import sys
import os
import sqlite3
from random import choice


class Player(pygame.sprite.Sprite):
    def __init__(self, id=0):
        super(Player, self).__init__()
        self.surf = load_image(f'./graphics/player/bird{id}.png')
        self.rect = self.surf.get_rect(center=(200, 250))
        self.velocity = 8
        self.direction = 1
        self.gravity = -15
        self.player_index = 0

    # Jump
    def apply_gravity(self):
        self.gravity += 1
        self.rect.y += self.gravity

    def apply_shift(self):
        self.rect.x += self.velocity * self.direction

    # Collision with floor or ceiling
    def death_check(self):
        global game_status, best_score, total_score, games_played
        if self.rect.top <= roof or self.rect.bottom >= floor:
            if score > best_score:
                best_score = score
            total_score += score
            games_played += 1
            update_stats()
            game_status = 'death_screen'

    def turn_check(self):
        global score, enemies, spikes
        if self.rect.right >= width or self.rect.left <= 0:
            self.direction *= -1
            self.surf = pygame.transform.flip(self.surf, True, False)
            score += 1
            spikes = []
            enemies = get_enemy_list()
            spawn_enemies(enemies)

    def draw(self):
        screen.blit(self.surf, self.rect)

    def update(self):
        self.apply_gravity()
        self.death_check()
        self.turn_check()
        self.apply_shift()
        self.draw()


def get_enemy_list():
    enemies = [choice([0, 1]) for i in range(10)]
    if enemies.count(0) <= 2 or enemies.count(0) >= 8:
        get_enemy_list()
    return enemies


def spawn_enemies(enemies):
    global spikes
    # if enemies:
    for i in range(10):
        enemy = enemies[i]
        if enemy:
            if bird.direction == 1:
                enemy_surf = right_spike_surf
                enemy_rect = right_spike_surf.get_rect(topright=(800, 50 * (i + 1)))
                # screen.blit(right_spike_surf, (800, 50 * i))
            else:
                enemy_surf = left_spike_surf
                enemy_rect = left_spike_surf.get_rect(topright=(50, 50 * (i + 1)))
                # screen.blit(left_spike_surf, (0, 50 * i))
            spikes.append(enemy_rect)
            screen.blit(enemy_surf, enemy_rect)


def collisions(player, spikes):
    global games_played, total_score, score, best_score, jumps_made
    if spikes:
        for spike_rect in spikes:
            if player.colliderect(spike_rect):
                games_played += 1
                total_score += score
                if score > best_score:
                    best_score = score
                update_statistic_values(best_score, games_played, total_score, jumps_made)
                update_stats()
                return True
    return False


def restart_game(new_status):
    global clock, game_status, spikes, enemies, score, current_skin_id, games_played
    # if new_status == 'active_game' or new_status == 'main_menu':
    #     games_played += 1
    bird.__init__(current_skin_id)
    game_status = new_status
    spikes = []
    score = 0
    enemies = get_enemy_list()
    spawn_enemies(enemies)


def load_image(name):
    dirname = os.path.dirname(__file__)
    fullname = os.path.join(dirname, name)
    try:
        image = pygame.image.load(fullname).convert_alpha()
    except pygame.error as message:
        print('Cannot load image:', name)
        raise SystemExit(message)
    return image


def update_stats():
    global con, cur, best_score, games_played, total_score, jumps_made
    cur.execute('''UPDATE stats
                SET best_score = ?,
                games_played = ?,
                total_score = ?,
                jumps_made = ?
                ''', (best_score, games_played, total_score, jumps_made))
    con.commit()


def update_statistic_values(best_score, games_played, total_score, jumps_made):
    global stats_label_surf, best_score_surf, games_played_surf, games_played_surf, total_score_surf, total_score_surf,\
        total_jumps_made_surf, stats_label_rect, best_score_rect, games_played_rect, games_played_rect,\
        total_score_rect, total_jumps_made_rect

    # Surfaces
    stats_label_surf = stats_label_font.render('Stats', False, '#ff3364')
    best_score_surf = best_score_font.render(f'Best score: {best_score}', False, '#ff3364')
    games_played_surf = games_played_font.render(f'Games played: {games_played}', False, '#ff3364')
    total_score_surf = total_score_font.render(f'Total score: {total_score}', False, '#ff3364')
    total_jumps_made_surf = total_jumps_made_font.render(f'Total jumps made: {jumps_made}', False, '#ff3364')
    # Rectangles
    stats_label_rect = stats_label_surf.get_rect(topleft=(10, 10))
    best_score_rect = best_score_surf.get_rect(topleft=(10, 145))
    games_played_rect = games_played_surf.get_rect(topleft=(10, 220))
    total_score_rect = total_score_surf.get_rect(topleft=(10, 295))
    total_jumps_made_rect = total_jumps_made_surf.get_rect(topleft=(10, 370))

    screen.blit(stats_label_surf, stats_label_rect)
    screen.blit(best_score_surf, best_score_rect)
    screen.blit(games_played_surf, games_played_rect)
    screen.blit(total_score_surf, total_score_rect)
    screen.blit(total_jumps_made_surf, total_jumps_made_rect)


def render_skins():
    global skin_objects
    for i in skin_objects:
        screen.blit(i[0], i[1])


def change_skin(pos):
    global skin_objects, current_skin_id, player
    for i, value in enumerate(skin_objects):
        if value[1].collidepoint(pos):
            current_skin_id = i
            for bird in player:
                bird.surf = load_image(f'./graphics/player/bird{i}.png')
            render_border()
            break


def render_border():
    global skin_objects, current_skin_id, all_cords
    pygame.draw.rect(
        screen,
        pygame.Color('#ff2b55'),
        (all_cords[current_skin_id][0] - 80, all_cords[current_skin_id][1] - 60, 160, 120),
        5
    )


# Database creating
if not os.path.isfile('touch-the-spikes-stats.db'):
    # Database connection
    con = sqlite3.connect('touch-the-spikes-stats.db')
    cur = con.cursor()
    cur.execute('''
               CREATE TABLE IF NOT EXISTS stats(
                   best_score INTEGER,
                   games_played INTEGER,
                   total_score INTEGER,
                   jumps_made INTEGER
               )''')
    con.commit()

    cur.execute('''
               INSERT OR IGNORE INTO stats(best_score, games_played, total_score, jumps_made)
               VALUES (?, ?, ?, ?)
           ''', (0, 0, 0, 0))
    con.commit()
else:
    con = sqlite3.connect('touch-the-spikes-stats.db')
    cur = con.cursor()

# Getting values from database
stats_data = cur.execute('''
                        SELECT * FROM stats 
''').fetchall()
best_score, games_played, total_score, jumps_made = stats_data[0]

# Window creation
pygame.init()
size = width, height = 800, 755
screen = pygame.display.set_mode(size)
screen.fill(pygame.Color('#aeaeae'))
clock = pygame.time.Clock()

# Creating skin objects
all_cords = ((165, 130), (400, 130), (635, 130), (165, 300), (400, 300), (635, 300), (165, 470), (400, 470), (635, 470))
skin_objects = []
current_skin_id = 0
for i in range(0, 9):
    bird_surf = pygame.transform.scale(load_image('./graphics/player/bird{}.png'.format(i)), (140, 100))
    bird_rect = bird_surf.get_rect(center=all_cords[i])
    skin_objects.append((bird_surf, bird_rect))

main_background_surf = load_image('./graphics/background/main_background.png')
main_background_rect = main_background_surf.get_rect(topleft=(0, 0))

# Buttons surfaces and rectangles
skin_menu_surf = load_image('./graphics/menu/btn_skins.png')
skin_menu_rect = skin_menu_surf.get_rect(center=(150, 677))

play_menu_surf = load_image('./graphics/menu/btn_play.png')
play_menu_rect = play_menu_surf.get_rect(center=(400, 677))

stats_menu_surf = load_image('./graphics/menu/btn_stats.png')
stats_menu_rect = stats_menu_surf.get_rect(center=(650, 677))

bottom_layer_surf = pygame.Surface((800, 200))
pygame.draw.rect(bottom_layer_surf, pygame.Color('#808080'), (0, 0, 800, 200))
bottom_layer_rect = bottom_layer_surf.get_rect(topleft=(0, 600))

# Score font
score_font = pygame.font.Font('./font/Pixeltype.ttf', 250)
score_surf = score_font.render('0', False, '#ff3364')
score_rect = score_surf.get_rect(center=(400, 300))

# Start game label font
start_game_font = pygame.font.Font('./font/Pixeltype.ttf', 50)
start_game_surf = start_game_font.render('Tap to jump', False, '#ff0000')
start_game_rect = start_game_surf.get_rect(center=(400, 350))

death_score_font = pygame.font.Font('./font/Pixeltype.ttf', 100)
death_score_surf = death_score_font.render('Score: 0', False, '#ff3364')
death_score_rect = death_score_surf.get_rect(center=(400, 250))

replay_button_font = pygame.font.Font('./font/Pixeltype.ttf', 100)
replay_button_surf = replay_button_font.render('Replay', False, '#ff0000')
replay_button_rect = replay_button_surf.get_rect(center=(400, 350))

go_to_menu_button_font = pygame.font.Font('./font/Pixeltype.ttf', 100)
go_to_menu_button_surf = go_to_menu_button_font.render('Menu', False, '#ff0000')
go_to_menu_button_rect = go_to_menu_button_surf.get_rect(center=(400, 450))

# Stats menu
# Fonts
stats_label_font = pygame.font.Font('./font/Pixeltype.ttf', 200)
best_score_font = pygame.font.Font('./font/Pixeltype.ttf', 100)
games_played_font = pygame.font.Font('./font/Pixeltype.ttf', 100)
total_score_font = pygame.font.Font('./font/Pixeltype.ttf', 100)
total_jumps_made_font = pygame.font.Font('./font/Pixeltype.ttf', 100)

update_statistic_values(best_score, games_played, total_score, jumps_made)

# Spikes fonts
left_spike_surf = load_image('./graphics/spike_25px.png')
left_spike_rect = left_spike_surf.get_rect(topleft=(0, 0))

right_spike_surf = pygame.transform.flip(left_spike_surf, True, False)
right_spike_rect = right_spike_surf.get_rect(topright=(800, 0))

player = pygame.sprite.GroupSingle()
bird = Player()
player.add(bird)

spikes = []
enemies = get_enemy_list()
spawn_enemies(enemies)

if __name__ == '__main__':
    score = 0
    running = True
    game_status = 'main_menu'
    roof, floor = 50, 500
    FPS = 60
    while running:
        pygame.display.set_caption('Touch the Spikes')

        if game_status != 'death_screen':
            screen.fill(pygame.Color('#aeaeae'))
            screen.blit(bottom_layer_surf, bottom_layer_rect)
            screen.blit(skin_menu_surf, skin_menu_rect)
            screen.blit(play_menu_surf, play_menu_rect)
            screen.blit(stats_menu_surf, stats_menu_rect)

        if game_status == 'main_menu':

            pygame.draw.rect(bottom_layer_surf, pygame.Color('#808080'), (0, 0, 800, 200))

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if main_background_rect.collidepoint(event.pos):
                        jumps_made += 1
                        bird.gravity = -15
                        restart_game('active_game')
                    elif skin_menu_rect.collidepoint(event.pos):
                        restart_game('skin_menu')
                    elif stats_menu_rect.collidepoint(event.pos):
                        restart_game('stats_menu')

            screen.blit(main_background_surf, main_background_rect)
            screen.blit(start_game_surf, start_game_rect)
            for bird in player:
                bird.draw()

        elif game_status == 'active_game':

            pygame.draw.rect(bottom_layer_surf, pygame.Color('#808080'), (0, 0, 800, 200))

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                    jumps_made += 1
                    bird.gravity = -15
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if main_background_rect.collidepoint(event.pos):
                        jumps_made += 1
                        bird.gravity = -15
                    elif skin_menu_rect.collidepoint(event.pos):
                        restart_game('skin_menu')
                    elif stats_menu_rect.collidepoint(event.pos):
                        restart_game('stats_menu')

            screen.blit(main_background_surf, main_background_rect)
            score_surf = score_font.render(str(score), False, '#ff3364')
            screen.blit(score_surf, score_rect)

            player.update()

            spawn_enemies(enemies)
            # Collision
            if collisions(bird.rect, spikes):
                game_status = 'death_screen'

        elif game_status == 'death_screen':
            screen.fill(pygame.Color('#aeaeae'))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if replay_button_rect.collidepoint(event.pos):
                        restart_game('active_game')

                    if go_to_menu_button_rect.collidepoint(event.pos):
                        restart_game('main_menu')

            death_score_surf = death_score_font.render(f'Score: {score}', False, '#ff3364')
            screen.blit(death_score_surf, death_score_rect)
            pygame.draw.rect(
                screen,
                pygame.Color('#808080'),
                (299 - 5, 319 - 5, 203 + 5, 63 + 5),
                border_radius=10
            )
            screen.blit(replay_button_surf, replay_button_rect)
            pygame.draw.rect(
                screen,
                pygame.Color('#808080'),
                (324 - 5, 419 - 5, 152 + 5, 63 + 5),
                border_radius=10
            )
            screen.blit(go_to_menu_button_surf, go_to_menu_button_rect)

        elif game_status == 'skin_menu':

            pygame.draw.rect(bottom_layer_surf, pygame.Color('#aeaeae'), (0, 0, 800, 200))

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if play_menu_rect.collidepoint(event.pos):
                        restart_game('main_menu')
                    elif stats_menu_rect.collidepoint(event.pos):
                        restart_game('stats_menu')
                    else:
                        change_skin(event.pos)

            render_skins()
            render_border()

        elif game_status == 'stats_menu':

            pygame.draw.rect(bottom_layer_surf, pygame.Color('#aeaeae'), (0, 0, 800, 200))

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if skin_menu_rect.collidepoint(event.pos):
                        restart_game('skin_menu')
                    elif play_menu_rect.collidepoint(event.pos):
                        restart_game('main_menu')


            update_statistic_values(best_score, games_played, total_score, jumps_made)
            update_stats()

        pygame.display.flip()
        clock.tick(FPS)
